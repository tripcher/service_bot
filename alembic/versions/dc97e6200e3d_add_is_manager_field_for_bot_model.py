"""Add is manager field for bot model

Revision ID: dc97e6200e3d
Revises: 9c8119b4ff72
Create Date: 2020-05-07 10:05:34.099224

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'dc97e6200e3d'
down_revision = '9c8119b4ff72'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('bots', sa.Column('is_manager', sa.Boolean(), server_default=sa.text('false'), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('bots', 'is_manager')
    # ### end Alembic commands ###
