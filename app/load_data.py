import os
import typing
import json
import pydantic

import app
from app.core import exceptions, db as database
from app.main.implement.shops import repositories as impl_rep


# FIXME: db=next(database.get_db()) какой то костыль, нужно разобраться с Dependency вне контроллера
def get_organization_repository(db=next(database.get_db())):
    return impl_rep.OrganizationSqlRepository(db)


def get_shop_repository(db=next(database.get_db())):
    return impl_rep.ShopSqlRepository(db)


def get_bot_repository(db=next(database.get_db())):
    return impl_rep.BotSqlRepository(db)


def get_group_repository(db=next(database.get_db())):
    return impl_rep.GroupSqlRepository(db)


def get_product_repository(db=next(database.get_db())):
    return impl_rep.ProductSqlRepository(db)


def print_log(event: str, model, entity: pydantic.BaseModel) -> None:
    print(
        '{event} model={model}, id={id}'.format(
            event=event,
            model=model,
            id=entity.id if hasattr(entity, 'id') else None  # type: ignore
        )
    )


class Loader(object):
    """Загрущик моделей БД из json."""

    PATH_DATA = os.path.join(app.BASE_DIR, 'data/group.json')

    def __init__(self, repository, path: str):
        self.repository = repository
        self.path: str = os.path.join(app.BASE_DIR, path)
        self.data: typing.Iterable[pydantic.BaseModel] = []

    def load_data(self) -> None:
        with open(self.path) as json_file:
            self.data = map(lambda item: self.repository.model(**item), json.load(json_file))  # type: ignore

        for entity in self.data:
            try:
                self.repository.save(entity, force_add=True)
            except exceptions.FailedAddObjectToCollection as exc:
                print_log(str(exc), self.repository.model, entity)

                try:
                    self.repository.save(entity)
                except exceptions.FailedEditObjectToCollection as exc:
                    print_log(str(exc), self.repository.model, entity)
                else:
                    print_log('Success edit', self.repository.model, entity)

            else:
                print_log('Success add', self.repository.model, entity)


if __name__ == "__main__":
    loaders = (
        Loader(
            repository=get_organization_repository(),
            path='data/organization.json'
        ),
        Loader(
            repository=get_shop_repository(),
            path='data/shop.json'
        ),
        Loader(
            repository=get_bot_repository(),
            path='data/bot.json'
        ),
        Loader(
            repository=get_group_repository(),
            path='data/group.json'
        ),
        Loader(
            repository=get_product_repository(),
            path='data/product.json'
        ),
    )

    for loader in loaders:
        print('Start load {model}'.format(model=loader.repository.model))
        loader.load_data()
        print('Finish load {model}'.format(model=loader.repository.model))

