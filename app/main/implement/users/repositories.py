import typing

from sqlalchemy import orm

from app.core import exceptions
from app.main.domain.users import models as domain_models, repositories
from app.main.implement.core import repositories as core_repositories
from app.main.implement.users import models


class UserSqlRepository(
    core_repositories.MixinSqlRepository[domain_models.User, models.UserSql],
    repositories.UserRepository
):
    model: typing.Type[domain_models.User] = domain_models.User
    sql_model: typing.Type[models.UserSql] = models.UserSql

    def get_for_id(self, platform_id: str) -> domain_models.User:
        platforms_func = (self.get_for_telegram_id,)

        # FIXME: нужно доработать на случай, если id платформ совпадают
        for func in platforms_func:
            try:
                return func(platform_id)
            except exceptions.ObjectDoesNotExist:
                pass
        else:
            raise exceptions.ObjectDoesNotExist(
                '{model} with platform_id = {platform_id} not found.'
                .format(model=self.sql_model.__name__, platform_id=platform_id)
            )

    def get_for_telegram_id(self, telegram_id: str) -> domain_models.User:
        try:
            entity = self.db.query(self.sql_model).filter(self.sql_model.telegram_id == telegram_id).one()
            return self._serialize(entity)  # type: ignore
        except orm.exc.NoResultFound:
            raise exceptions.ObjectDoesNotExist(
                '{model} with telegram_id = {telegram_id} not found.'
                .format(model=self.sql_model.__name__, telegram_id=telegram_id)
            )
        except orm.exc.MultipleResultsFound:
            raise exceptions.MultipleObjectsReturned(
                '{model} with telegram_id = {telegram_id} more than one were found.'
                .format(model=self.sql_model.__name__, telegram_id=telegram_id)
            )
