import sqlalchemy as sql
from sqlalchemy.dialects import postgresql

from app.core import db
from app.main.implement.core import models


class UserSql(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, db.Base):
    """Модель пользователя."""
    __tablename__ = 'users'

    id = sql.Column(postgresql.UUID(as_uuid=True), primary_key=True, index=True,
                    server_default=sql.text("uuid_generate_v4()"))
    telegram_id = sql.Column(sql.String(255), unique=True, index=True)
    telegram_login = sql.Column(sql.String(255), nullable=True)
    telegram_chat_id = sql.Column(sql.String(255), nullable=False, unique=True)
    bot_names = sql.Column(sql.ARRAY(sql.String))
