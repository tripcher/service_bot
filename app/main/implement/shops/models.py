import sqlalchemy as sql
from sqlalchemy import orm

from app.core import db
from app.main.domain.shops import const
from app.main.implement.core import models


class OrganizationSql(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, db.Base):
    """Модель организации."""
    __tablename__ = 'organization'

    id = sql.Column(sql.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    is_active = sql.Column(sql.Boolean, default=True)

    managers = orm.relationship('ManagerSql', back_populates='organization')
    shops = orm.relationship('ShopSql', back_populates='organization')


class ShopSql(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, db.Base):
    """Модель заведения (ресторан, магазин, кофейня и т.д.)."""
    __tablename__ = 'shops'

    id = sql.Column(sql.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    key = sql.Column(sql.String(32), unique=True, index=True, nullable=False)
    name = sql.Column(sql.String(255), nullable=False)
    organization_id = sql.Column(sql.Integer, sql.ForeignKey('organization.id'), nullable=False)

    bot = orm.relationship('BotSql', uselist=False, back_populates='shop')
    organization = orm.relationship('OrganizationSql', back_populates='shops')
    managers = orm.relationship('ManagerSql', back_populates='shop')
    groups = orm.relationship('GroupSql', back_populates='shop')


class BotSql(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, db.Base):
    """Модель бота."""
    __tablename__ = 'bots'

    id = sql.Column(sql.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    name = sql.Column(sql.String(255), unique=True)
    api_key = sql.Column(sql.String(32), unique=True, index=True, nullable=False)
    web_hook = sql.Column(sql.String(2083), unique=True, nullable=False)
    telegram_token = sql.Column(sql.String(255), unique=True, nullable=False)
    shop_id = sql.Column(sql.Integer, sql.ForeignKey('shops.id'), nullable=False)
    is_manager = sql.Column(sql.Boolean, nullable=False, server_default=sql.false(), default=False)

    shop = orm.relationship('ShopSql', back_populates='bot')

    __table_args__ = (sql.UniqueConstraint('shop_id', 'is_manager'),)


class ManagerSql(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, db.Base):
    """Модель менеджера."""
    __tablename__ = 'managers'

    id = sql.Column(sql.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    first_name = sql.Column(sql.String(255), nullable=True)
    last_name = sql.Column(sql.String(255), nullable=True)
    telegram_id = sql.Column(sql.String(255), unique=True, index=True, nullable=True)
    telegram_login = sql.Column(sql.String(255), nullable=True)
    telegram_chat_id = sql.Column(sql.String(255), nullable=True, unique=True)
    organization_id = sql.Column(sql.Integer, sql.ForeignKey('organization.id'), nullable=False)
    shop_id = sql.Column(sql.Integer, sql.ForeignKey('shops.id'), nullable=True)

    organization = orm.relationship('OrganizationSql', back_populates='managers')
    shop = orm.relationship('ShopSql', back_populates='managers')


class GroupSql(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, db.Base):
    """Модель группы продуктов (группа в меню)."""
    __tablename__ = 'groups'

    id = sql.Column(sql.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    text = sql.Column(sql.String(255), nullable=False)
    parent_id = sql.Column(sql.Integer, sql.ForeignKey('groups.id'), nullable=True)
    shop_id = sql.Column(sql.Integer, sql.ForeignKey('shops.id'), nullable=False)
    first = sql.Column(sql.Boolean, nullable=False, default=False)
    additive = sql.Column(sql.Boolean, nullable=False, default=False)

    shop = orm.relationship('ShopSql', back_populates='groups')
    parent = orm.relationship('GroupSql', remote_side=[id], backref='children')
    products = orm.relationship('ProductSql', back_populates='group')


class ProductSql(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, db.Base):
    """Модель продукта."""
    __tablename__ = 'products'

    id = sql.Column(sql.Integer, primary_key=True, index=True, unique=True, autoincrement=True)
    text = sql.Column(sql.String(255))
    price = sql.Column(sql.Float, nullable=True)
    volume = sql.Column(sql.Integer, nullable=True)
    dimension = sql.Column(sql.Enum(const.Dimension), nullable=True)
    group_id = sql.Column(sql.Integer, sql.ForeignKey('groups.id'), nullable=False)

    group = orm.relationship('GroupSql', back_populates='products')
