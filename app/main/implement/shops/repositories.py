import typing

from sqlalchemy import orm

from app.core import exceptions
from app.main.domain.shops import models as domain_models, repositories
from app.main.implement.core import repositories as core_repositories
from app.main.implement.shops import models


class OrganizationSqlRepository(
    core_repositories.MixinSqlRepository[domain_models.Organization, models.OrganizationSql],
    repositories.OrganizationRepository
):
    model: typing.Type[domain_models.Organization] = domain_models.Organization
    sql_model: typing.Type[models.OrganizationSql] = models.OrganizationSql


class ShopSqlRepository(
    core_repositories.MixinSqlRepository[domain_models.Shop, models.ShopSql],
    repositories.ShopRepository
):
    model: typing.Type[domain_models.Shop] = domain_models.Shop
    sql_model: typing.Type[models.ShopSql] = models.ShopSql

    def get_for_organization(self, organization_pk: int) -> typing.List[domain_models.Shop]:
        entities = self.all_active().filter(self.sql_model.organization_id == organization_pk)
        return self._serialize(entities, many=True)  # type: ignore


class ManagerSqlRepository(
    core_repositories.MixinSqlRepository[domain_models.Manager, models.ManagerSql],
    repositories.ManagerRepository
):
    model: typing.Type[domain_models.Manager] = domain_models.Manager
    sql_model: typing.Type[models.ManagerSql] = models.ManagerSql

    def get_for_organization(self, organization_pk: int, is_all: bool = True) -> typing.List[domain_models.Manager]:
        pass

    def get_for_shop(self, shop_pk: int) -> typing.List[domain_models.Manager]:
        pass

    def get_for_telegram_id(self, telegram_id: str) -> domain_models.Manager:
        try:
            entity = self.db.query(self.sql_model).filter(self.sql_model.telegram_id == telegram_id).one()
            return self._serialize(entity)  # type: ignore
        except orm.exc.NoResultFound:
            raise exceptions.ObjectDoesNotExist(
                '{model} with telegram_id = {telegram_id} not found.'
                .format(model=self.sql_model.__name__, telegram_id=telegram_id)
            )
        except orm.exc.MultipleResultsFound:
            raise exceptions.MultipleObjectsReturned(
                '{model} with telegram_id = {telegram_id} more than one were found.'
                .format(model=self.sql_model.__name__, telegram_id=telegram_id)
            )


class BotSqlRepository(
    core_repositories.MixinSqlRepository[domain_models.Bot, models.BotSql],
    repositories.BotRepository
):
    model: typing.Type[domain_models.Bot] = domain_models.Bot
    sql_model: typing.Type[models.BotSql] = models.BotSql

    def get_for_shop(self, shop_pk: int) -> typing.List[domain_models.Bot]:
        entities = self.all_active().filter(self.sql_model.shop_id == shop_pk)
        return self._serialize(entities, many=True)  # type: ignore

    def get_client_for_shop(self, shop_pk: int) -> domain_models.Bot:
        entities = self.all_active().filter(
            self.sql_model.shop_id == shop_pk,
            self.sql_model.is_manager.is_(False)
        ).one()
        return self._serialize(entities)  # type: ignore

    def get_manager_for_shop(self, shop_pk: int) -> domain_models.Bot:
        entities = self.all_active().filter(
            self.sql_model.shop_id == shop_pk,
            self.sql_model.is_manager.is_(True)
        ).one()
        return self._serialize(entities)  # type: ignore

    def get_for_key(self, api_key: str) -> domain_models.Bot:
        try:
            entity = self.all_active().filter(self.sql_model.api_key == api_key).one()
            return self._serialize(entity)  # type: ignore
        except orm.exc.NoResultFound:
            raise exceptions.ObjectDoesNotExist(
                '{model} with api_key = {api_key} not found.'.format(model=self.sql_model.__name__, api_key=api_key)
            )
        except orm.exc.MultipleResultsFound:
            raise exceptions.MultipleObjectsReturned(
                '{model} with api_key = {api_key} more than one were found.'.format(
                    model=self.sql_model.__name__,
                    api_key=api_key
                )
            )


class GroupSqlRepository(
    core_repositories.MixinSqlRepository[domain_models.Group, models.GroupSql],
    repositories.GroupRepository
):
    model: typing.Type[domain_models.Group] = domain_models.Group
    sql_model: typing.Type[models.GroupSql] = models.GroupSql

    def get_for_shop(self, shop_id: int, group_id: int) -> domain_models.Group:
        try:
            entity = self.all_active()\
                .filter(self.sql_model.id == group_id, self.sql_model.shop_id == shop_id).one()
            return self._serialize(entity)  # type: ignore
        except orm.exc.NoResultFound:
            raise exceptions.ObjectDoesNotExist(
                '{model} with id = {id}, shop_id = {shop_id} not found.'.format(
                    model=self.sql_model.__name__,
                    id=id,
                    shop_id=shop_id
                )
            )
        except orm.exc.MultipleResultsFound:
            raise exceptions.MultipleObjectsReturned(
                '{model} with id = {id}, shop_id = {shop_id} more than one were found.'.format(
                    model=self.sql_model.__name__,
                    id=id,
                    shop_id=shop_id
                )
            )

    def get_all_for_shop(self, shop_id: int) -> typing.List[domain_models.Group]:
        pass

    def get_first_level_for_shop(self, shop_id: int) -> typing.List[domain_models.Group]:
        entities = self.all_active().filter(
            self.sql_model.shop_id == shop_id,
            self.sql_model.first,
            self.sql_model.additive.is_(False),
            self.sql_model.parent_id.is_(None)
        )
        return self._serialize(entities, many=True)  # type: ignore

    def get_first_level_additive_for_shop(self, shop_id: int) -> typing.List[domain_models.Group]:
        entities = self.all_active().filter(
            self.sql_model.shop_id == shop_id,
            self.sql_model.first,
            self.sql_model.additive,
            self.sql_model.parent_id.is_(None)
        )
        return self._serialize(entities, many=True)  # type: ignore

    def get_for_parent(self, shop_id: int, parent_id: int) -> typing.List[domain_models.Group]:
        entities = self.all_active().filter(
            self.sql_model.shop_id == shop_id,
            self.sql_model.parent_id == parent_id,
            self.sql_model.additive.is_(False)
        )
        return self._serialize(entities, many=True)  # type: ignore


class ProductSqlRepository(
    core_repositories.MixinSqlRepository[domain_models.Product, models.ProductSql],
    repositories.ProductRepository
):
    model: typing.Type[domain_models.Product] = domain_models.Product
    sql_model: typing.Type[models.ProductSql] = models.ProductSql

    def get_for_shop(self, shop_id: int, product_id: int) -> domain_models.Product:
        try:
            entity = self.all_active().join(self.sql_model.group) \
                .filter(self.sql_model.id == product_id, models.GroupSql.shop_id == shop_id).one()
            return self._serialize(entity)  # type: ignore
        except orm.exc.NoResultFound:
            raise exceptions.ObjectDoesNotExist(
                '{model} with id = {id}, shop_id = {shop_id} not found.'.format(
                    model=self.sql_model.__name__,
                    id=id,
                    shop_id=shop_id
                )
            )
        except orm.exc.MultipleResultsFound:
            raise exceptions.MultipleObjectsReturned(
                '{model} with id = {id}, shop_id = {shop_id} more than one were found.'.format(
                    model=self.sql_model.__name__,
                    id=id,
                    shop_id=shop_id
                )
            )

    def get_all_for_shop(self, shop_id: int) -> typing.List[domain_models.Product]:
        entities = self.all_active().join(self.sql_model.group).filter(
            models.GroupSql.shop_id == shop_id
        )
        return self._serialize(entities, many=True)  # type: ignore

    def get_for_group(self, shop_id: int, group_id: int) -> typing.List[domain_models.Product]:
        entities = self.all_active().join(self.sql_model.group).filter(
            self.sql_model.group_id == group_id,
            models.GroupSql.shop_id == shop_id
        )
        return self._serialize(entities, many=True)  # type: ignore
