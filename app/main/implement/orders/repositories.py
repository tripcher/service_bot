import typing

import pydantic

from app.main.domain.orders import models as domain_models, repositories, const
from app.main.implement.core import repositories as core_repositories
from app.main.implement.orders import models


class OrderSqlRepository(
    core_repositories.MixinSqlRepository[domain_models.Order, models.OrderSql],
    repositories.OrderRepository
):
    model: typing.Type[domain_models.Order] = domain_models.Order
    sql_model: typing.Type[models.OrderSql] = models.OrderSql

    def get_all_for_shop(self, shop_id: int) -> typing.List[domain_models.Order]:
        entities = self.all_active().filter(self.sql_model.shop_id == shop_id)
        return self._serialize(entities, many=True)  # type: ignore

    def get_for_status(self, shop_id: int, status: const.OrderStatus) -> typing.List[domain_models.Order]:
        entities = self.all_active().filter(self.sql_model.shop_id == shop_id, self.sql_model.status == status)
        return self._serialize(entities, many=True)  # type: ignore

    def get_active_order_for_user(self, shop_id: int, user_id: pydantic.UUID4) -> typing.Optional[domain_models.Order]:
        entity = self.all_active().filter(
            self.sql_model.shop_id == shop_id,
            self.sql_model.user_id == user_id,
            self.sql_model.status != const.OrderStatus.STATUS_CLOSED
        ).order_by(self.sql_model.create_datetime.desc()).first()

        if entity is None:
            return None

        return self._serialize(entity)  # type: ignore
