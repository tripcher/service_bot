import sqlalchemy as sql
from sqlalchemy import orm
from sqlalchemy.dialects import postgresql

from app.core import db
from app.main.domain.orders import const
from app.main.implement.core import models


class OrderSql(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, db.Base):
    """Модель заказа."""
    __tablename__ = 'orders'

    id = sql.Column(postgresql.UUID(as_uuid=True), primary_key=True, index=True, unique=True,
                     server_default=sql.text("uuid_generate_v4()"))
    sum = sql.Column(sql.Float)
    status = sql.Column(sql.Enum(const.OrderStatus))
    products = sql.Column(sql.JSON)
    additives = sql.Column(sql.JSON)
    shop_id = sql.Column(sql.Integer, sql.ForeignKey('shops.id'))
    user_id = sql.Column(postgresql.UUID(as_uuid=True), sql.ForeignKey('users.id'))

    shop = orm.relationship('ShopSql', backref='orders')
    user = orm.relationship('UserSql', backref='orders')
