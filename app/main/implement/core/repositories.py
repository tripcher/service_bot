import typing

import pydantic
from sqlalchemy import orm, exc as sql_exc

from app.core import db, exceptions
from app.main.domain.core import repositories

T = typing.TypeVar('T')
D = typing.TypeVar('D')


class MixinSqlRepository(typing.Generic[T, D]):

    model: typing.Type[pydantic.BaseModel]
    sql_model: typing.Type[db.Base]

    def __init__(self, db: orm.Session):
        self.db = db

    def _serialize(
            self,
            entities: typing.Union[D, typing.List[D]],
            many: bool = False
    ) -> typing.Union[T, typing.List[T]]:

        if many:
            return [self.model.from_orm(entity) for entity in entities]  # type: ignore

        return self.model.from_orm(entities)  # type: ignore

    def all_active(self) -> orm.query.Query:
        if hasattr(self.sql_model, 'deleted'):
            entities = self.db.query(self.sql_model).filter(self.sql_model.deleted.is_(False))
        else:
            entities = self.db.query(self.sql_model).all()
        return entities

    def get(self, pk: repositories.TYPE_ID) -> T:
        try:
            entity = self.all_active().filter(self.sql_model.id == pk).one()  # type: ignore
            return self._serialize(entity)  # type: ignore
        except orm.exc.NoResultFound:
            raise exceptions.ObjectDoesNotExist(
                '{model} with pk = {pk} not found.'.format(model=self.sql_model.__name__, pk=pk)
            )
        except orm.exc.MultipleResultsFound:
            raise exceptions.MultipleObjectsReturned(
                '{model} with pk = {pk} more than one were found.'.format(model=self.sql_model.__name__, pk=pk)
            )

    def _add(self, entity_dict: typing.Dict[str, typing.Any], commit: bool) -> T:
        db_entity = self.sql_model(**entity_dict)
        try:
            self.db.add(db_entity)
            if commit:
                self.db.commit()
                self.db.refresh(db_entity)
        except sql_exc.SQLAlchemyError as exc:
            self.db.rollback()
            print(exc)
            raise exceptions.FailedAddObjectToCollection('{model} failed add.'.format(model=self.sql_model.__name__))

        return self._serialize(db_entity)  # type: ignore

    def _edit(self, entity_dict: typing.Dict[str, typing.Any], commit: bool) -> None:
        try:
            self.db.query(self.sql_model)\
                .filter(self.sql_model.id == entity_dict.pop('id', None))\
                .update(entity_dict)
            if commit:
                self.db.commit()
        except (exceptions.ObjectDoesNotExist, exceptions.MultipleObjectsReturned) as exc:
            raise exceptions.FailedEditObjectToCollection()
        except sql_exc.SQLAlchemyError as exc:
            self.db.rollback()
            raise exceptions.FailedEditObjectToCollection('{model} failed edit.'.format(model=self.sql_model.__name__))

    def save(self, entity: T, commit: bool = True, force_add: bool = False) -> T:
        # TODO: доработать relationship

        entity_dict = entity.dict()  # type: ignore

        if entity.id is None or force_add:  # type: ignore
            return self._add(entity_dict, commit=commit)

        self._edit(entity_dict, commit=commit)

        return entity

    def delete(self, entity: T) -> None:
        pass
