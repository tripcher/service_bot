from datetime import datetime
import sqlalchemy as sql


class MixinCreateDatetime(object):

    create_datetime = sql.Column(sql.DateTime, default=datetime.utcnow)


class MixinUpdateDatetime(object):

    update_datetime = sql.Column(sql.DateTime, default=datetime.utcnow)

    def __setattr__(self, key, value):
        # FIXME: подумать как сделать правилней
        self.__dict__['update_datetime'] = datetime.utcnow()
        self.__dict__[key] = value


class MixinCreateAndUpdateDatetime(MixinCreateDatetime, MixinUpdateDatetime):
    pass


class MixinDeleted(object):

    deleted = sql.Column(sql.Boolean, default=False)
