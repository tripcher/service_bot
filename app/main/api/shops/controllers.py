import typing
from fastapi import APIRouter, Depends

from app.core import db as database
from app.main.domain.shops import services, models as domain_models, repositories as domain_rep
from app.main.implement.shops import repositories as impl_rep
from app.main.api import auth

router = APIRouter()


def get_group_repository(db=Depends(database.get_db)):
    return impl_rep.GroupSqlRepository(db)


def get_product_repository(db=Depends(database.get_db)):
    return impl_rep.ProductSqlRepository(db)


@router.get(
    '/groups/{group_id}',
    response_model=domain_models.Group
)
async def get_group_for_shop(
        group_id,
        current_bot: domain_models.Bot = Depends(auth.current_bot),
        group_rep: domain_rep.GroupRepository = Depends(get_group_repository),
):
    return services.groups.get_for_shop(group_id, current_bot, group_rep)


@router.get('/groups', response_model=typing.List[domain_models.Group])
async def get_groups_for_shop(
        first: bool = False,
        additive: bool = False,
        parent_id: typing.Optional[int] = None,
        current_bot: domain_models.Bot = Depends(auth.current_bot),
        group_rep: domain_rep.GroupRepository = Depends(get_group_repository)
):
    return services.groups.get_list_for_shop(first, additive, parent_id, current_bot, group_rep)


@router.get(
    '/products/{product_id}',
    response_model=domain_models.Product
)
async def get_product_for_shop(
        product_id,
        current_bot: domain_models.Bot = Depends(auth.current_bot),
        product_rep: domain_rep.ProductRepository = Depends(get_product_repository),
):
    return services.products.get_for_shop(product_id, current_bot, product_rep)


@router.get('/products', response_model=typing.List[domain_models.Product])
async def get_products_for_group(
        group_id: typing.Optional[int] = None,
        current_bot: domain_models.Bot = Depends(auth.current_bot),
        product_rep: domain_rep.ProductRepository = Depends(get_product_repository),
):
    return services.products.get_list(group_id, current_bot, product_rep)
