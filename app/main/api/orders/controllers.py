import typing

import pydantic
from fastapi import APIRouter, Depends

from app.core import db as database, exceptions
from app.main.api import exceptions as api_exceptions
from app.main.domain.shops import models as shops_models,  repositories as shops_domain_rep
from app.main.domain.orders import services, repositories as domain_rep, schemas, models, const, \
    exceptions as order_exceptions
from app.main.implement.orders import repositories as impl_rep
from app.main.implement.shops import repositories as shops_impl_rep
from app.main.domain.users import repositories as users_domain_rep
from app.main.implement.users import repositories as users_impl_rep
from app.main.api import auth

router = APIRouter()


def get_user_repository(db=Depends(database.get_db)):
    return users_impl_rep.UserSqlRepository(db)


def get_order_repository(db=Depends(database.get_db)):
    return impl_rep.OrderSqlRepository(db)


def get_bot_repository(db=Depends(database.get_db)):
    return shops_impl_rep.BotSqlRepository(db)


@router.post('/', response_model=models.Order)
async def create(
        data: schemas.OrderBase,
        current_bot: shops_models.Bot = Depends(auth.current_bot),
        order_rep: domain_rep.OrderRepository = Depends(get_order_repository),
        user_rep: users_domain_rep.UserRepository = Depends(get_user_repository)
):
    return services.save(data, current_bot, order_rep, user_rep, force_add=True)


@router.put('/', response_model=models.Order)
async def update(
        data: schemas.OrderBase,
        current_bot: shops_models.Bot = Depends(auth.current_bot),
        order_rep: domain_rep.OrderRepository = Depends(get_order_repository),
        user_rep: users_domain_rep.UserRepository = Depends(get_user_repository)
):
    return services.save(data, current_bot, order_rep, user_rep)


@router.post('/send-to-manager', response_model=models.Order)
async def send_to_manager(
        data: schemas.OrderBase,
        current_bot: shops_models.Bot = Depends(auth.current_bot),
        order_rep: domain_rep.OrderRepository = Depends(get_order_repository),
        user_rep: users_domain_rep.UserRepository = Depends(get_user_repository)
) -> models.Order:
    """
    Отправляет заказ менеджеру. Сохраняет заказ.
    Args:
        data (schemas.OrderBase):
        current_bot (shops_models.Bot):
        order_rep (domain_rep.OrderRepository):
        user_rep (users_domain_rep.UserRepository):

    Returns:
        models.Order:

    Raises:
        FailedAddObjectToCollection: Не удаль добавить объект в коллекцию.
        FailedEditObjectToCollection: Не удалось обновить объект в коллекции.
        ObjectDoesNotExist: Пользователь не найден.
        OrderStateMethodException:

    """
    return services.send_to_manager(data, current_bot, order_rep, user_rep)


@router.post('/{order_id}/accept-from-manager', response_model=models.Order)
async def accept_from_manager(
        data: schemas.AcceptOrder,
        order_id: pydantic.UUID4,
        current_bot: shops_models.Bot = Depends(auth.current_bot),
        current_manager: shops_models.Manager = Depends(auth.current_manager),
        order_rep: domain_rep.OrderRepository = Depends(get_order_repository),
        bot_rep: shops_domain_rep.BotRepository = Depends(get_bot_repository),
):
    try:
        return services.accept_from_manager(data, order_id, current_bot, current_manager, order_rep, bot_rep)
    except exceptions.PermissionDenied as exc:
        raise api_exceptions.ManagerPermissionDeniedHTTPException()
    except order_exceptions.OrderStateMethodException as exc:
        raise api_exceptions.NotValidOperationHTTPException()


@router.post('/{order_id}/denied-from-manager', response_model=models.Order)
async def denied_from_manager(
        data: schemas.AcceptOrder,
        order_id: pydantic.UUID4,
        current_bot: shops_models.Bot = Depends(auth.current_bot),
        current_manager: shops_models.Manager = Depends(auth.current_manager),
        order_rep: domain_rep.OrderRepository = Depends(get_order_repository),
):
    try:
        return services.denied_from_manager(data, order_id, current_bot, current_manager, order_rep)
    except exceptions.PermissionDenied:
        raise api_exceptions.ManagerPermissionDeniedHTTPException()
    except order_exceptions.OrderStateMethodException:
        raise api_exceptions.NotValidOperationHTTPException()


@router.post('/{order_id}/stop-list-from-manager', response_model=models.Order)
async def stop_list_from_manager(
        data: schemas.AcceptOrder,
        order_id: pydantic.UUID4,
        current_bot: shops_models.Bot = Depends(auth.current_bot),
        current_manager: shops_models.Manager = Depends(auth.current_manager),
        order_rep: domain_rep.OrderRepository = Depends(get_order_repository),
):
    try:
        return services.stop_list_from_manager(data, order_id, current_bot, current_manager, order_rep)
    except exceptions.PermissionDenied:
        raise api_exceptions.ManagerPermissionDeniedHTTPException()
    except order_exceptions.OrderStateMethodException:
        raise api_exceptions.NotValidOperationHTTPException()


@router.get('/active/{user_id}', response_model=models.Order)
async def get_active_order_for_user(
        user_id: str,
        current_bot: shops_models.Bot = Depends(auth.current_bot),
        order_rep: domain_rep.OrderRepository = Depends(get_order_repository),
        user_rep: users_domain_rep.UserRepository = Depends(get_user_repository)
):
    order = services.get_active_order_for_user(user_id, current_bot, order_rep, user_rep)

    if order is None:
        raise api_exceptions.NotFoundHTTPException

    return order


@router.get('/', response_model=typing.List[models.Order])
async def get_orders_for_shop(
        status: const.OrderStatusKey = None,
        current_bot: shops_models.Bot = Depends(auth.current_bot),
        current_manager: shops_models.Manager = Depends(auth.current_manager),
        order_rep: domain_rep.OrderRepository = Depends(get_order_repository)
):
    if status is not None:
        current_status: const.OrderStatus = const.OrderStatus[status]
    try:
        return services.get_list_for_shop(current_status, current_bot, current_manager, order_rep)
    except exceptions.PermissionDenied:
        raise api_exceptions.ManagerPermissionDeniedHTTPException()
