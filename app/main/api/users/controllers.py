from fastapi import APIRouter, Depends

from app.core import db as database
from app.main.domain.shops import models as shops_models
from app.main.domain.users import services, repositories as domain_rep, schemas
from app.main.implement.users import repositories as impl_rep
from app.main.api import auth

router = APIRouter()


def get_user_repository(db=Depends(database.get_db)):
    return impl_rep.UserSqlRepository(db)


@router.post('/',)
async def save(
        data: schemas.UserBase,
        current_bot: shops_models.Bot = Depends(auth.current_bot),
        user_rep: domain_rep.UserRepository = Depends(get_user_repository)
):
    return services.save(data, current_bot, user_rep)
