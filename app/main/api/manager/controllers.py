from fastapi import APIRouter, Depends

from app.core import db as database, schemas
from app.main.api import exceptions as api_exceptions
from app.main.domain.shops import services, schemas as shops_schemas, models, repositories as domain_rep
from app.main.implement.shops import repositories as impl_rep

from app.main.api import auth

router = APIRouter()


def get_manager_repository(db=Depends(database.get_db)):
    return impl_rep.ManagerSqlRepository(db)


@router.post('/auth',)
async def post_auth(
        data: shops_schemas.ManagerBase,
        current_bot: models.Bot = Depends(auth.current_bot),
        manager_rep: domain_rep.ManagerRepository = Depends(get_manager_repository),
):
    is_auth = services.managers.auth(data, current_bot, manager_rep)
    if not is_auth:
        raise api_exceptions.AuthManagerHTTPException()

    return schemas.Success()
