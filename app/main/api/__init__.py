from fastapi import APIRouter

from .shops import router as shop_router
from .users import router as user_router
from .orders import router as order_router
from .manager import router as manager_router


__all__ = ('router_v1',)

router_v1 = APIRouter()

router_v1.include_router(
    shop_router,
    prefix="/shops",
    tags=["shops"]
)

router_v1.include_router(
    user_router,
    prefix="/users",
    tags=["users"]
)

router_v1.include_router(
    order_router,
    prefix="/orders",
    tags=["orders"]
)

router_v1.include_router(
    manager_router,
    prefix="/managers",
    tags=["managers"]
)
