import enum
from fastapi import exceptions, status


class KeysError(str, enum.Enum):
    """Ключи ошибок."""

    AUTH_BOT_ERROR = 'AUTH_BOT_ERROR'
    AUTH_MANAGER_ERROR = 'AUTH_MANAGER_ERROR'
    AUTH_PLATFORM_USER_ERROR = 'AUTH_PLATFORM_USER_ERROR'
    MANAGER_PERMISSION_DENIED = 'MANAGER_PERMISSION_DENIED'
    NOT_VALID_OPERATION = 'NOT_VALID_OPERATION'
    NOT_FOUND = 'NOT_FOUND'


class NotFoundHTTPException(exceptions.HTTPException):
    def __init__(self) -> None:
        super().__init__(status_code=status.HTTP_404_NOT_FOUND, detail=KeysError.NOT_FOUND)


class NotValidOperationHTTPException(exceptions.HTTPException):
    def __init__(self) -> None:
        super().__init__(status_code=status.HTTP_400_BAD_REQUEST, detail=KeysError.NOT_VALID_OPERATION)


class AuthBotHTTPException(exceptions.HTTPException):
    def __init__(self) -> None:
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=KeysError.AUTH_BOT_ERROR)


class AuthManagerHTTPException(exceptions.HTTPException):
    def __init__(self) -> None:
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=KeysError.AUTH_MANAGER_ERROR)


class AuthPlatformUserHTTPException(exceptions.HTTPException):
    def __init__(self) -> None:
        super().__init__(status_code=status.HTTP_401_UNAUTHORIZED, detail=KeysError.AUTH_PLATFORM_USER_ERROR)


class ManagerPermissionDeniedHTTPException(exceptions.HTTPException):
    def __init__(self) -> None:
        super().__init__(status_code=status.HTTP_403_FORBIDDEN, detail=KeysError.MANAGER_PERMISSION_DENIED)
