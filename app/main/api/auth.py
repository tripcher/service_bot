import typing

import jwt
from fastapi import Depends, Security
from fastapi.security import APIKeyHeader as BaseAPIKeyHeader
from starlette.requests import Request

from app.core import db as database, config, exceptions
from app.main.api import exceptions as api_exceptions
from app.main.domain.shops import repositories as domain_rep, models as domain_models
from app.main.implement.shops import repositories as impl_rep
from app.main.implement.users import repositories as impl_user_rep


class APIKeyHeader(BaseAPIKeyHeader):
    async def __call__(self, request: Request) -> typing.Optional[str]:
        api_key: str = request.headers.get(self.model.name)
        if not api_key:
            if self.auto_error:
                raise api_exceptions.AuthBotHTTPException
            else:
                return None
        return api_key


class ManagerIdHeader(BaseAPIKeyHeader):
    async def __call__(self, request: Request) -> typing.Optional[str]:
        api_key: str = request.headers.get(self.model.name)
        if not api_key:
            if self.auto_error:
                raise api_exceptions.AuthManagerHTTPException
            else:
                return None
        return api_key


X_API_KEY = APIKeyHeader(name='X-API-Key')
X_MANAGER_ID = ManagerIdHeader(name='X-Manager-Id')


async def get_bot_repository(db=Depends(database.get_db)):
    return impl_rep.BotSqlRepository(db)


async def get_user_repository(db=Depends(database.get_db)):
    return impl_user_rep.UserSqlRepository(db)


async def get_manager_repository(db=Depends(database.get_db)):
    return impl_rep.ManagerSqlRepository(db)


async def current_bot(
        bot_rep: domain_rep.BotRepository = Depends(get_bot_repository),
        x_api_key: str = Security(X_API_KEY)
) -> domain_models.Bot:
    """
    Декодирует и проверяет JWT, возвращает объект бота.
    Args:
        bot_rep (repositories.BotRepository): Репозиторий ботов.
        x_api_key (str): jwt.

    Returns:
        domain_models.Bot:

    Raises:
        AuthBotHTTPException:
        NotFoundHTTPException:
    """

    try:
        decoded: typing.Dict[str, str] = jwt.decode(x_api_key, config.JWT_KEY, algorithms='HS256')
    except jwt.PyJWTError:
        raise api_exceptions.AuthBotHTTPException()

    try:
        key = decoded.get('key')

        if key is None:
            raise api_exceptions.AuthBotHTTPException()

        return bot_rep.get_for_key(key)
    except exceptions.ObjectDoesNotExist:
        raise api_exceptions.AuthBotHTTPException()


# async def current_user(
#         user_rep: domain_user_rep.UserRepository = Depends(get_user_repository),
#         x_platform_user: str = Security(X_PLATFORM_USER)
# ) -> domain_user_models.User:
#     """
#     Проверяет существует ли данный пользователь, возвращает пользователя.
#     Args:
#         user_rep (repositories.BotRepository):
#         x_platform_user (str): platform id.
#
#     Returns:
#         domain_user_models.User:
#
#     Raises:
#         AuthBotHTTPException:
#         NotFoundHTTPException:
#     """
#
#     try:
#         user = user_rep.get_for_id(x_platform_user)
#     except exceptions.ObjectDoesNotExist:
#         raise exceptions.AuthPlatformUserHTTPException()
#     else:
#         return user


async def current_manager(
        manager_rep: domain_rep.ManagerRepository = Depends(get_manager_repository),
        x_manager_id: str = Security(X_MANAGER_ID)
) -> domain_models.Manager:
    """
    Проверяет существует ли данный менеджер, возвращает менеджера.
    Args:
        manager_rep (repositories.ManagerRepository):
        x_manager_id (str): telegram id.

    Returns:
        domain_models.Manager:

    Raises:
        AuthBotHTTPException:
        NotFoundHTTPException:
    """

    try:
        manager = manager_rep.get_for_telegram_id(x_manager_id)
    except exceptions.ObjectDoesNotExist:
        raise api_exceptions.AuthManagerHTTPException()
    else:
        return manager
