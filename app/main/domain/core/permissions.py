from app.core import exceptions
from app.main.domain.shops import models as domain_shops_models


def manager_has_permission(
        current_bot: domain_shops_models.Bot,
        current_manager: domain_shops_models.Manager,
        raise_exceptions: bool = False,
) -> bool:
    """
    Проверяет права менеджера на магазин текущего бота.
    Args:
        current_bot (domain_shops_models.Bot):
        current_manager (domain_shops_models.Manager):
        raise_exceptions (bool): Вызывать исключение?

    Returns:
        bool:

    Raises:
        PermissionDenied:
    """
    if current_bot.shop_id != current_manager.shop_id:
        if raise_exceptions:
            raise exceptions.PermissionDenied()
        return False
    return True
