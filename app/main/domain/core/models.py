from datetime import datetime


class MixinCreateDatetime(object):

    create_datetime: datetime = datetime.utcnow()


class MixinUpdateDatetime(object):

    update_datetime: datetime = datetime.utcnow()

    def __setattr__(self, key, value):
        self.__dict__['update_datetime'] = datetime.utcnow()
        self.__dict__[key] = value


class MixinCreateAndUpdateDatetime(MixinCreateDatetime, MixinUpdateDatetime):
    pass


class MixinDeleted(object):

    deleted: bool = False
