import abc
import typing

import pydantic

T = typing.TypeVar('T')
TYPE_ID = typing.Union[int, pydantic.UUID4, str]


class MixinRepository(abc.ABC, typing.Generic[T]):

    @abc.abstractmethod
    def get(self, pk: TYPE_ID) -> T:
        """
        Возвращает сущность.
        Args:
            pk (TYPE_ID): Идентификатор сущности.

        Returns:
            T: Сущность.

        Raises:
            ObjectDoesNotExist: Сущность не найдена.
            MultipleObjectsReturned: Найдено больше одной сущности.
        """
        pass

    @abc.abstractmethod
    def save(self, entity: T, commit: bool = True, force_add: bool = False) -> T:
        """
        Сохраняет сущность в коллекцию.
        Если идентификатор не найден, добавляет сущность в коллекци.
        Если идентификатор существует, то обновляет данную сущность в коллекции.
        Args:
            entity (Т): Сущность.
            commit (bool): Соммит транзакции.
            force_add (bool): Принудительно добавить сущность.

        Returns:
            None:

        Raises:
            FailedAddObjectToCollection: Не удалось добавить объект в коллекцию.
            FailedEditObjectToCollection: Не удалось обновить объект в коллекции.
        """
        pass

    @abc.abstractmethod
    def delete(self, entity: T) -> None:
        """
        Удалить сущность из коллекции.
        Args:
            entity (Т): Сущность.

        Returns:
            None:

        Raises:
            FailedDeleteObjectToCollection: Не удаль удалить объект из коллекции.
        """
        pass
