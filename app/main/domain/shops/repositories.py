import abc
import typing

from app.main.domain.core import repositories
from app.main.domain.shops import models


class OrganizationRepository(repositories.MixinRepository[models.Organization], abc.ABC):
    """Коллекция организаций."""
    pass


class ShopRepository(repositories.MixinRepository[models.Shop], abc.ABC):
    """Коллекция магазинов."""

    @abc.abstractmethod
    def get_for_organization(self, organization_pk: int) -> typing.List[models.Shop]:
        """
        Возвращает список магазинов, которые принадлежат организации.
        Args:
            organization_pk (int): Идентификатор организации.

        Returns:
            typing.List[models.Shop]: Список магазинов.
        """
        pass


class BotRepository(repositories.MixinRepository[models.Bot], abc.ABC):
    """Коллекция магазинов."""

    @abc.abstractmethod
    def get_for_key(self, api_key: str) -> models.Bot:
        """
        Возвращает бота магазина.
        Args:
            api_key (str): API ключ бота.

        Returns:
            models.Bot: Бот.

        Raises:
            ObjectDoesNotExist: Сущность не найдена.
            MultipleObjectsReturned: Найдено больше одной сущности.
        """
        pass

    @abc.abstractmethod
    def get_for_shop(self, shop_pk: int) -> typing.List[models.Bot]:
        """
        Возвращает ботов магазина.
        Args:
            shop_pk (int): Идентификатор магазина.

        Returns:
            typing.List[models.Bot]:
        """
        pass

    @abc.abstractmethod
    def get_client_for_shop(self, shop_pk: int) -> models.Bot:
        """
        Возвращает клиентского бота магазина.
        Args:
            shop_pk (int): Идентификатор магазина.

        Returns:
            models.Bot:
        """
        pass

    @abc.abstractmethod
    def get_manager_for_shop(self, shop_pk: int) -> models.Bot:
        """
        Возвращает административного бота магазина.
        Args:
            shop_pk (int): Идентификатор магазина.

        Returns:
            models.Bot:
        """
        pass


class ManagerRepository(repositories.MixinRepository[models.Manager], abc.ABC):
    """Коллекция менеджеров."""

    @abc.abstractmethod
    def get_for_organization(self, organization_pk: int, is_all: bool = True) -> typing.List[models.Manager]:
        """
        Возвращает список менеджеров организации.
        Если is_all=True, то возвращает менеджеров организации и менеджеров мазаников, которые принадлежат организации.
        Если is_all=False, то возвращает только менеджеров организации.
        Args:
            organization_pk (int): Идентификатор организации.
            is_all (bool): Вернуть всех менеджеров, включая менеджеров магазинов?

        Returns:
            typing.List[models.Manager]: Список менеджеров.
        """
        pass

    @abc.abstractmethod
    def get_for_shop(self, shop_pk: int) -> typing.List[models.Manager]:
        """
        Возвращает список менеджеров магазина.
        Args:
            shop_pk (int): Идентификатор магазина.

        Returns:
            typing.List[models.Manager]: Список менеджеров.
        """
        pass

    @abc.abstractmethod
    def get_for_telegram_id(self, telegram_id: str) -> models.Manager:
        """
        Возвращает менеджера.
        Args:
            telegram_id (str): Идентификатор пользователя в телеграме.

        Returns:
            models.Manager: Менеджер.

        Raises:
            ObjectDoesNotExist: Сущность не найдена.
            MultipleObjectsReturned: Найдено больше одной сущности.
        """
        pass


class GroupRepository(repositories.MixinRepository[models.Group], abc.ABC):
    """Коллекция групп."""

    @abc.abstractmethod
    def get_for_shop(self, shop_id: int, group_id: int) -> models.Group:
        """
        Возвращает группу магазина.
        Args:
            shop_id (int): Идентификатор магазина.
            group_id (int): Идентификатор группы.

        Returns:
            models.Group: Группа.

        Raises:
            ObjectDoesNotExist: Сущность не найдена.
            MultipleObjectsReturned: Найдено больше одной сущности.
        """
        pass

    @abc.abstractmethod
    def get_all_for_shop(self, shop_id: int) -> typing.List[models.Group]:
        """
        Возвращает все группы магазина.
        Args:
            shop_id (int): Идентификатор магазина.

        Returns:
            typing.List[models.Group]: Список групп.
        """
        pass

    @abc.abstractmethod
    def get_first_level_for_shop(self, shop_id: int) -> typing.List[models.Group]:
        """
        Возвращает группы магазина для первого уровня меню.

        Должны выполняться условия:
        first = True
        parent = None
        additive = False

        Args:
            shop_id (int): Идентификатор магазина.

        Returns:
            typing.List[models.Manager]: Список групп.
        """
        pass

    @abc.abstractmethod
    def get_first_level_additive_for_shop(self, shop_id: int) -> typing.List[models.Group]:
        """
        Возвращает группы магазина для первого уровня меню добавок.

        Должны выполняться условия:
        first = True
        parent = None
        additive = True

        Args:
            shop_id (int): Идентификатор магазина.

        Returns:
            typing.List[models.Manager]: Список групп.
        """
        pass

    @abc.abstractmethod
    def get_for_parent(self, shop_id: int, parent_id: int) -> typing.List[models.Group]:
        """
        Возвращает все подгруппы группы.
        Args:
            shop_id (int): Идентификатор магазина.
            parent_id (int): Идентификатор группы.

        Returns:
            typing.List[models.Manager]: Список групп.
        """
        pass


class ProductRepository(repositories.MixinRepository[models.Product], abc.ABC):
    """Коллекция продуктов."""

    def get_for_shop(self, shop_id: int, product_id: int) -> models.Product:
        """
        Возвращает продукт для магазина.
        Args:
            shop_id (int): Идентификатор магазина.
            product_id (int): Идентификатор продукта.

        Returns:
            models.Product: Продукт.
        """
        pass

    def get_all_for_shop(self, shop_id: int) -> typing.List[models.Product]:
        """
        Возвращает спиок всех продуктов для магазина.
        Args:
            shop_id (int): Идентификатор магазина.

        Returns:
            typing.List[models.Product]: Список продуктов.
        """
        pass

    def get_for_group(self, shop_id: int, group_id: int) -> typing.List[models.Product]:
        """
        Возвращает спиок продуктов для группы.
        Args:
            shop_id (int): Идентификатор магазина.
            group_id (int): Идентификатор группы.

        Returns:
            typing.List[models.Product]: Список продуктов.
        """
        pass
