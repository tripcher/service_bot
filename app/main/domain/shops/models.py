import typing
import pydantic
import secrets

from app.main.domain.core import models
from app.main.domain.shops import const


class Organization(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, pydantic.BaseModel):
    """Модель организации."""

    id: typing.Optional[int] = None
    is_active: bool = True

    class Config:
        orm_mode = True


class Shop(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, pydantic.BaseModel):
    """Модель заведения (ресторан, магазин, кофейня и т.д.)."""

    id: typing.Optional[int] = None
    key: str = pydantic.Field(default=secrets.token_hex(16), max_length=32)
    name: str = pydantic.Field(..., max_length=255)
    organization_id: int

    organization: typing.Optional['Organization'] = None

    class Config:
        orm_mode = True

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'organization'}
        return super(Shop, self).dict(*args, **kwargs)


class Bot(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, pydantic.BaseModel):
    """Модель бота."""

    id: typing.Optional[int] = None
    name: str = pydantic.Field(..., max_length=255)
    api_key: str = pydantic.Field(default=secrets.token_hex(16), max_length=32)
    web_hook: pydantic.HttpUrl
    telegram_token: str = pydantic.Field(..., max_length=255)
    shop_id: int
    is_manager: bool = False

    shop: typing.Optional['Shop'] = None

    class Config:
        orm_mode = True

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'shop'}
        return super(Bot, self).dict(*args, **kwargs)


class Manager(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, pydantic.BaseModel):
    """Модель менеджера."""

    id: typing.Optional[int] = None
    first_name: typing.Optional[str] = pydantic.Field(default=None, max_length=255)
    last_name: typing.Optional[str] = pydantic.Field(default=None, max_length=255)
    telegram_id: typing.Optional[str] = pydantic.Field(default=None, max_length=255)
    telegram_login: typing.Optional[str] = pydantic.Field(default=None, max_length=255)
    telegram_chat_id: typing.Optional[str] = pydantic.Field(default=None, max_length=255)
    shop_id: typing.Optional[int]
    organization_id: int

    organization: typing.Optional['Organization']
    shop: typing.Optional['Shop']

    class Config:
        orm_mode = True

    @property
    def is_active(self) -> bool:
        """
        Менеджер активен?
        Returns:
            bool:
        """

        return not self.deleted

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'organization', 'shop'}
        return super(Manager, self).dict(*args, **kwargs)


class Group(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, pydantic.BaseModel):

    """Модель группы продуктов (группа в меню)."""

    id: typing.Optional[int] = None
    text: str = pydantic.Field(..., max_length=255)
    # флаг для обозначения групп, которые выводятся в главном меню
    first: bool = False
    # флаг для обозначения группы добавок, группа с True должна быть только одна
    additive: bool = False
    parent_id: typing.Optional[int] = None
    shop_id: int

    shop: typing.Optional['Shop'] = None
    # parent: typing.Optional['Group']

    class Config:
        orm_mode = True

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'shop'}
        return super(Group, self).dict(*args, **kwargs)


class Product(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, pydantic.BaseModel):
    """Модель продукта."""

    id: typing.Optional[int] = None
    text: str = pydantic.Field(..., max_length=255)
    price: typing.Optional[float] = None
    volume: typing.Optional[int] = None
    dimension: typing.Optional[const.Dimension] = None
    group_id: int

    group: typing.Optional['Group'] = None

    class Config:
        orm_mode = True

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'group'}
        return super(Product, self).dict(*args, **kwargs)
