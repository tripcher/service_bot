import enum


@enum.unique
class Dimension(str, enum.Enum):
    """Размерность."""

    ML = 'мл'
    GR = 'гр'
    KG = 'кг'
