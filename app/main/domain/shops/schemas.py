import typing

import pydantic


class ManagerBase(pydantic.BaseModel):
    telegram_id: str = pydantic.Field(..., max_length=255)
    telegram_login: typing.Optional[str] = pydantic.Field(default=None, max_length=255)
    telegram_chat_id: str = pydantic.Field(..., max_length=255)
    auth_key: str = pydantic.Field(..., max_length=32)
