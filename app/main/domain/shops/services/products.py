import typing

from app.main.domain.shops import models, repositories


def get_for_shop(
        product_id: int,
        current_bot: models.Bot,
        product_rep: repositories.ProductRepository,
) -> models.Product:
    """
    Возвращает продукт.
    Args:
        product_id (int): Идентификатор продукта.
        current_bot (models.Bot): Текущий бот.
        product_rep (repositories.ProductRepository):

    Returns:
        models.Product:
    """
    return product_rep.get_for_shop(current_bot.shop_id, product_id)


def get_list(
        group_id: typing.Optional[int],
        current_bot: models.Bot,
        product_rep: repositories.ProductRepository,
) -> typing.List[models.Product]:
    """
    Возвращает продукты для магазина.
    Args:
        group_id (int): Возвращает все продукты для родительской группы.
        current_bot (models.Bot): Текущий бот.
        product_rep (repositories.ProductRepository):

    Returns:
        typing.Iterable[models.Product]:
    """
    if group_id is not None:
        return product_rep.get_for_group(current_bot.shop_id, group_id)

    return product_rep.get_all_for_shop(current_bot.shop_id)
