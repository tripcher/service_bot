import typing

from app.main.domain.shops import models, repositories


def get_for_shop(
        group_id: int,
        current_bot: models.Bot,
        group_rep: repositories.GroupRepository,
) -> models.Group:
    """
    Возвращает группу.
    Args:
        group_id (int): Идентификатор группы.
        current_bot (models.Bot): Текущий бот.
        group_rep (repositories.GroupRepository):

    Returns:
        models.Group:
    """
    return group_rep.get_for_shop(current_bot.shop_id, group_id)


def get_list_for_shop(
        first: bool,
        additive: bool,
        parent_id: typing.Optional[int],
        current_bot: models.Bot,
        group_rep: repositories.GroupRepository
) -> typing.List[models.Group]:
    """
    Возвращает все группы магазина.
    Args:
        first (bool): Возвращает группы магазина для первого уровня меню.
        additive (bool): Возвращает группы добавок магазина для первого уровня меню.
        parent_id (int): Возвращает группы для родительской группы.
        current_bot (models.Bot): Текущий бот.
        group_rep (repositories.GroupRepository):

    Returns:
        typing.List[models.Group]:
    """
    if first and additive:
        return group_rep.get_first_level_additive_for_shop(current_bot.shop_id)

    if first:
        return group_rep.get_first_level_for_shop(current_bot.shop_id)

    if parent_id is not None:
        return group_rep.get_for_parent(current_bot.shop_id, parent_id)

    return group_rep.get_all_for_shop(current_bot.shop_id)
