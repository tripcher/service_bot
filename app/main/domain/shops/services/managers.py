from app.core import exceptions
from app.main.domain.shops import models, repositories, schemas


def auth(
        data: schemas.ManagerBase,
        current_bot: models.Bot,
        manager_rep: repositories.ManagerRepository,
) -> bool:
    """
    Проверяет auth_key и сохраняет пользователя.
    Args:
        data (schemas.ManagerBase):
        current_bot (models.Bot): Текущий бот.
        manager_rep (repositories.ManagerRepository):

    Returns:
        bool: Успешная авторизация и сохранение.
    """

    data_dict = data.dict()

    if current_bot.shop.key != data_dict.pop('auth_key'):  # type: ignore
        return False
    try:
        manager = manager_rep.get_for_telegram_id(data.telegram_id)
    except exceptions.ObjectDoesNotExist:
        try:
            manager_rep.save(
                models.Manager(  # type: ignore
                    **data_dict,
                    shop_id=current_bot.shop_id,
                    organization_id=current_bot.shop.organization_id  # type: ignore
                )
            )
        except (exceptions.FailedAddObjectToCollection, exceptions.FailedEditObjectToCollection):
            return False
    else:
        # FIXME: менеджер может быть привязан только к одному магазину, при каждом входе в разные магазины,
        #  магазин перезаписывается, сделано для того что бы можно было легко перезаписать вход в демо бота.
        manager.telegram_chat_id = data.telegram_chat_id
        manager.shop_id = current_bot.shop_id
        manager.organization_id = current_bot.shop.organization_id  # type: ignore
        manager_rep.save(manager)

    return True
