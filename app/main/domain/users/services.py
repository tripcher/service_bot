from app.core import exceptions
from app.main.domain.shops import models as shops_models
from app.main.domain.users import models, repositories, schemas


def save(
        data: schemas.UserBase,
        current_bot: shops_models.Bot,
        user_rep: repositories.UserRepository
) -> None:
    """
    Сохраняет пользователя.
    Args:
        data (schemas.UserBase):
        current_bot (shops_models.Bot):
        user_rep (repositories.UserRepository):

    Returns:
        None

    Raises:
        FailedAddObjectToCollection: Не удаль добавить объект в коллекцию.
        FailedEditObjectToCollection: Не удалось обновить объект в коллекции.
    """
    try:
        user = user_rep.get_for_id(data.platform_id)
        if current_bot.name not in user.bot_names:
            user.bot_names.append(current_bot.name)
            user.telegram_chat_id = data.platform_chat_id
            user_rep.save(user)
    except exceptions.ObjectDoesNotExist:
        user = models.User(
            telegram_id=data.platform_id,
            telegram_login=data.platform_login,
            telegram_chat_id=data.platform_chat_id,
            bot_names=[current_bot.name]
        )
        user_rep.save(user)
