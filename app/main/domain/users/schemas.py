import typing

import pydantic


class UserBase(pydantic.BaseModel):
    platform_id: str = pydantic.Field(..., max_length=255)
    platform_login: typing.Optional[str] = pydantic.Field(default=None, max_length=255)
    platform_chat_id: str = pydantic.Field(..., max_length=255)
