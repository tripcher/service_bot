import abc

from app.main.domain.core import repositories
from app.main.domain.users import models


class UserRepository(repositories.MixinRepository[models.User], abc.ABC):
    """Коллекция пользователей."""

    @abc.abstractmethod
    def get_for_id(self, platform_id: str) -> models.User:
        """
            Возвращает пользователя.
            Args:
                platform_id (str): Идентификатор пользователя в платформе (телеграм, вк и т.д.).

            Returns:
                models.User: Пользователь.

            Raises:
                ObjectDoesNotExist: Сущность не найдена.
                MultipleObjectsReturned: Найдено больше одной сущности.
            """
        pass

    @abc.abstractmethod
    def get_for_telegram_id(self, telegram_id: str) -> models.User:
        """
        Возвращает пользователя.
        Args:
            telegram_id (str): Идентификатор пользователя в телеграме.

        Returns:
            models.User: Пользователь.

        Raises:
            ObjectDoesNotExist: Сущность не найдена.
            MultipleObjectsReturned: Найдено больше одной сущности.
        """
        pass
