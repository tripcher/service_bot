import typing

import pydantic

from app.main.domain.core import models


class User(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, pydantic.BaseModel):
    """Модель пользователя."""

    id: typing.Optional[pydantic.UUID4] = None
    telegram_id: str = pydantic.Field(..., max_length=255)
    telegram_login: typing.Optional[str] = pydantic.Field(default=None, max_length=255)
    bot_names: typing.List[str]
    telegram_chat_id: str = pydantic.Field(..., max_length=255)

    class Config:
        orm_mode = True
