import abc
import typing

import pydantic

from app.main.domain.core import repositories
from app.main.domain.orders import models, const


class OrderRepository(repositories.MixinRepository[models.Order], abc.ABC):
    """Коллекция заказов."""

    @abc.abstractmethod
    def get_all_for_shop(self, shop_id: int) -> typing.List[models.Order]:
        """
        Возвращает заказы магазина.
        Args:
            shop_id (int): Идентификатор магазина.

        Returns:
            typing.List[models.Order]
        """
        pass

    @abc.abstractmethod
    def get_for_status(self, shop_id: int, status: const.OrderStatus) -> typing.List[models.Order]:
        """
        Возвращает заказы магазина в определенном статусе.
        Args:
            shop_id (int): Идентификатор магазина.
            status (const.OrderStatus): Статус.

        Returns:
            typing.List[models.Order]:
        """
        pass

    @abc.abstractmethod
    def get_active_order_for_user(self, shop_id: int, user_id: pydantic.UUID4) -> typing.Optional[models.Order]:
        """
        Возвращает активный заказ пользователя (status != STATUS_CLOSED).

        Если у пользователя несколько заказов, то возвращаем последний.
        Args:
            shop_id (int): Идентификатор магазина.
            user_id (user_id: pydantic.UUID4):

        Returns:
            typing.Optional[models.Order]
        """
        pass
