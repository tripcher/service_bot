class OrderStateMethodException(Exception):

    def __init__(self, method: str, status: str):
        _msg = 'You cannot call the \"{method}\" method on an order with the status \"{status}\".'
        super(OrderStateMethodException, self).__init__(_msg.format(method=method, status=status))


class OrderFail(Exception):
    pass


class StateContextFail(Exception):
    pass
