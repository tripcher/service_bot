import typing

import pydantic

from app.main.domain.shops import models as shop_models
from app.main.domain.orders import const


class OrderBase(pydantic.BaseModel):
    id: pydantic.UUID4
    sum: float
    status: const.OrderStatus
    products: typing.List['shop_models.Product']
    additives: typing.Dict[int, typing.List['shop_models.Product']]
    user_id: str = pydantic.Field(..., max_length=255)


class AcceptOrder(pydantic.BaseModel):
    comment: typing.Optional[str] = pydantic.Field(default=None, strip_whitespace=True)


class UserMessage(pydantic.BaseModel):
    platform_id: str = pydantic.Field(..., max_length=255)
    platform_chat_id: str = pydantic.Field(..., max_length=255)
    comment: typing.Optional[str] = pydantic.Field(default=None, strip_whitespace=True)
