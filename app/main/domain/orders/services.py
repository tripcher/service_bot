import typing

import pydantic
from app.core import exceptions
from app.main.domain.core import permissions
from app.main.domain.users import repositories as users_repositories
from app.main.domain.shops import models as shops_models, repositories as shops_repositories
from app.main.domain.orders import models, repositories, schemas, const, exceptions as orders_exceptions


def save(
        data: schemas.OrderBase,
        current_bot: shops_models.Bot,
        order_rep: repositories.OrderRepository,
        user_rep: users_repositories.UserRepository,
        force_add: bool = False
) -> models.Order:
    """
    Сохраняет заказ.
    Args:
        data (schemas.OrderBase):
        current_bot (shops_models.Bot):
        order_rep (repositories.OrderRepository):
        user_rep (users_repositories.UserRepository):
        force_add(bool):

    Returns:
        None

    Raises:
        FailedAddObjectToCollection: Не удаль добавить объект в коллекцию.
        FailedEditObjectToCollection: Не удалось обновить объект в коллекции.
        ObjectDoesNotExist: Пользователь не найден.
    """

    data_dict = data.dict()
    try:
        user = user_rep.get_for_id(data_dict.pop('user_id'))
    except exceptions.ObjectDoesNotExist:
        # TODO: нужно сделать исключения для моделей как в джанге
        raise

    order = models.Order(  # type: ignore
        **data_dict,
        user_id=user.id,  # type: ignore
        shop_id=current_bot.shop_id
    )

    order = order_rep.save(order, force_add=force_add)

    return order


def send_to_manager(
        data: schemas.OrderBase,
        current_bot: shops_models.Bot,
        order_rep: repositories.OrderRepository,
        user_rep: users_repositories.UserRepository
) -> models.Order:
    """
    Отправляет заказ менеджеру. Сохраняет заказ.
    Args:
        data (schemas.OrderBase):
        current_bot (shops_models.Bot):
        order_rep (repositories.OrderRepository):
        user_rep (users_repositories.UserRepository):

    Returns:
        None

    Raises:
        FailedAddObjectToCollection: Не удаль добавить объект в коллекцию.
        FailedEditObjectToCollection: Не удалось обновить объект в коллекции.
        ObjectDoesNotExist: Пользователь не найден.
        OrderStateMethodException:
    """

    data_dict = data.dict()
    try:
        user = user_rep.get_for_id(data_dict.pop('user_id'))
    except exceptions.ObjectDoesNotExist:
        # TODO: нужно сделать исключения для моделей как в джанге
        raise

    order = models.Order(  # type: ignore
        **data_dict,
        user_id=user.id,  # type: ignore
        shop_id=current_bot.shop_id
    )

    order.send_to_managers()

    order = order_rep.save(order)

    return order


def accept_from_manager(
        data: schemas.AcceptOrder,
        order_id: pydantic.UUID4,
        current_bot: shops_models.Bot,
        current_manager: shops_models.Manager,
        order_rep: repositories.OrderRepository,
        bot_rep: shops_repositories.BotRepository
) -> models.Order:
    """
    Менеджер подтверждает заказ.
    Args:
        data (schemas.AcceptOrder):
        order_id (pydantic.UUID4):
        current_bot (shops_models.Bot):
        current_manager (shops_models.Manager):
        order_rep (repositories.OrderRepository):
        bot_rep (shops_repositories.BotRepository):

    Returns:
        models.Order

    Raises:
        PermissionDenied:
        OrderStateMethodException:
    """

    permissions.manager_has_permission(current_bot, current_manager, raise_exceptions=True)

    order = order_rep.get(order_id)
    client_bot = bot_rep.get_client_for_shop(order.shop_id)
    order.accept_from_manager(current_manager, client_bot, comment=data.comment)
    order_rep.save(order)

    return order


def denied_from_manager(
        data: schemas.AcceptOrder,
        order_id: pydantic.UUID4,
        current_bot: shops_models.Bot,
        current_manager: shops_models.Manager,
        order_rep: repositories.OrderRepository,
) -> models.Order:
    """
    Менеджер отклоняет заказ.
    Args:
        data (schemas.AcceptOrder):
        order_id (pydantic.UUID4):
        current_bot (shops_models.Bot):
        current_manager (shops_models.Manager):
        order_rep (repositories.OrderRepository):

    Returns:
        models.Order

    Raises:
        PermissionDenied:
        OrderStateMethodException:
    """

    permissions.manager_has_permission(current_bot, current_manager, raise_exceptions=True)

    order = order_rep.get(order_id)
    order.denied_from_manager(current_manager, comment=data.comment)
    order_rep.save(order)

    return order


def stop_list_from_manager(
        data: schemas.AcceptOrder,
        order_id: pydantic.UUID4,
        current_bot: shops_models.Bot,
        current_manager: shops_models.Manager,
        order_rep: repositories.OrderRepository,
) -> models.Order:
    """
    Менеджер отклоняет заказ (так как товар в стоп листе).
    Args:
        data (schemas.AcceptOrder):
        order_id (pydantic.UUID4):
        current_bot (shops_models.Bot):
        current_manager (shops_models.Manager):
        order_rep (repositories.OrderRepository):

    Returns:
        models.Order

    Raises:
        PermissionDenied:
        OrderStateMethodException:
    """

    permissions.manager_has_permission(current_bot, current_manager, raise_exceptions=True)

    order = order_rep.get(order_id)
    order.stop_list_from_manager(current_manager, comment=data.comment)
    order_rep.save(order)

    return order


def get_list_for_shop(
        status: typing.Optional[const.OrderStatus],
        current_bot: shops_models.Bot,
        current_manager: shops_models.Manager,
        order_rep: repositories.OrderRepository
) -> typing.List[models.Order]:
    """
    Возвращает все заказы магазина.
    Args:
        status (const.OrderStatus): Статус заказа, возвращает заказы в данном статусе.
        current_bot (models.Bot): Текущий бот.
        current_manager (shops_models.Manager):
        order_rep (repositories.OrderRepository):

    Returns:
        typing.List[models.Order]:

    Raises:
        PermissionDenied:
    """
    permissions.manager_has_permission(current_bot, current_manager, raise_exceptions=True)

    if status is not None:
        return order_rep.get_for_status(current_bot.shop_id, status)
    return order_rep.get_all_for_shop(current_bot.shop_id)


def get_active_order_for_user(
        platform_id: str,
        current_bot: shops_models.Bot,
        order_rep: repositories.OrderRepository,
        user_rep: users_repositories.UserRepository
) -> typing.Optional[models.Order]:
    try:
        user = user_rep.get_for_id(platform_id)
    except exceptions.ObjectDoesNotExist:
        return None

    if user.id is None:
        return None

    return order_rep.get_active_order_for_user(current_bot.shop_id, user.id)
