import abc
import typing

from app.main import requests
from app.main.domain.orders import decorators, models, const, exceptions, schemas
from app.main.domain.shops import models as shop_models

F = typing.TypeVar('F', bound=typing.Callable[..., typing.Any])


class OrderState(abc.ABC):
    """Состояние заказа."""

    status: const.OrderStatus

    _context: 'models.Order'

    @property
    def context(self) -> 'models.Order':
        return self._context

    @context.setter
    def context(self, context: 'models.Order') -> None:
        self._context = context

    def _raise_exception(self, method: str):
        raise exceptions.OrderStateMethodException(method, self.status)

    @abc.abstractmethod
    def add_product(self, product: 'shop_models.Product') -> None:
        pass

    @abc.abstractmethod
    def delete_product(self, product: 'shop_models.Product') -> None:
        pass

    @abc.abstractmethod
    def send_to_managers(self) -> None:
        pass

    @abc.abstractmethod
    def accept_from_manager(self, manager: 'shop_models.Manager', bot: 'shop_models.Bot', comment: str = None) -> None:
        """
        Подтвердить заказ.
        Args:
            manager (shop_models.Manager): Менеджер.
            bot (shop_models.Bot): Бот клиента.
            comment (str): Комментарий.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        pass

    @abc.abstractmethod
    def denied_from_manager(self, manager: 'shop_models.Manager', comment: str = None) -> None:
        pass

    @abc.abstractmethod
    def stop_list_from_manager(self, manager: 'shop_models.Manager', comment: str = None) -> None:
        pass

    @abc.abstractmethod
    def fail(self, error: str = None, raises_exceptions: bool = False) -> None:
        pass

    @abc.abstractmethod
    def ready(self, comment: str = None) -> None:
        pass

    @abc.abstractmethod
    def close(self, comment: str = None) -> None:
        pass


class BaseOrderState(OrderState):

    def add_product(self, product: 'shop_models.Product') -> None:
        self._raise_exception('add_product')

    def delete_product(self, product: 'shop_models.Product') -> None:
        self._raise_exception('delete_product')

    def send_to_managers(self) -> None:
        self._raise_exception('sent_to_manager')

    def accept_from_manager(self, manager: 'shop_models.Manager', bot: 'shop_models.Bot', comment: str = None) -> None:
        self._raise_exception('accept_from_manager')

    def denied_from_manager(self, manager: 'shop_models.Manager', comment: str = None) -> None:
        self._raise_exception('denied_from_manager')

    def stop_list_from_manager(self, manager: 'shop_models.Manager', comment: str = None) -> None:
        self._raise_exception('stop_list_from_manager')

    def fail(self, error: str = None, raises_exceptions: bool = False) -> None:
        self.context.set_state(FailureState())
        if raises_exceptions:
            raise exceptions.OrderFail(error)

    def ready(self, comment: str = None) -> None:
        self._raise_exception('ready')

    def close(self, comment: str = None) -> None:
        self._raise_exception('close')


class NotSelectedState(BaseOrderState):

    status = const.OrderStatus.STATUS_NOT_SELECTED

    def add_product(self, product: 'shop_models.Product') -> None:
        self.context.products.append(product)
        self.context.set_state(SelectedState())

    def delete_product(self, product: 'shop_models.Product') -> None:
        pass


class SelectedState(BaseOrderState):

    status = const.OrderStatus.STATUS_SELECTED

    def add_product(self, product: 'shop_models.Product') -> None:
        if product.deleted:
            # TODO: raise exception
            pass
        self.context.products.append(product)

    def delete_product(self, product: 'shop_models.Product') -> None:
        self.context.products.remove(product)

        if len(self.context.products) == 0:
            self.context.set_state(NotSelectedState())

    @decorators.fail_state_method_error
    def send_to_managers(self) -> None:
        # TODO: дергаем метод отправки сообщения менеджерам магазина
        self.context.set_state(SentState())


class SentState(BaseOrderState):

    status = const.OrderStatus.STATUS_SENT

    @decorators.has_perm_manager
    @decorators.fail_state_method_error
    @decorators.fail_http_error
    def accept_from_manager(self, manager: 'shop_models.Manager', bot: 'shop_models.Bot', comment: str = None) -> None:
        data = schemas.UserMessage(
            platform_id=self.context.user.telegram_id,  # type: ignore
            platform_chat_id=self.context.user.telegram_chat_id,  # type: ignore
            comment=comment
        )

        response = requests.client.accept_from_manager(data=data.dict(), base_url=bot.web_hook)

        if response.get('status') != const.OrderStatus.STATUS_ACCEPT:
            self.fail(error='Client server returned status not equal STATUS_ACCEPT')
        else:
            self.context.set_state(AcceptState())

    @decorators.has_perm_manager
    @decorators.fail_state_method_error
    def denied_from_manager(self, manager: 'shop_models.Manager', comment: str = None) -> None:
        # TODO: дергаем метод отправки сообщения пользователю
        self.context.set_state(DeniedState())

    @decorators.has_perm_manager
    @decorators.fail_state_method_error
    def stop_list_from_manager(self, manager: 'shop_models.Manager', comment: str = None) -> None:
        # TODO: дергаем метод отправки сообщения пользователю
        self.context.set_state(StopListState())


class AcceptState(BaseOrderState):

    status = const.OrderStatus.STATUS_ACCEPT

    def ready(self, comment: str = None) -> None:
        # TODO: дергаем метод отправки сообщения пользователю
        self.context.set_state(ReadyState())


class DeniedState(BaseOrderState):

    status = const.OrderStatus.STATUS_DENIED


class StopListState(BaseOrderState):

    status = const.OrderStatus.STATUS_STOP_LIST


class FailureState(BaseOrderState):

    status = const.OrderStatus.STATUS_FAILURE


class ReadyState(BaseOrderState):

    status = const.OrderStatus.STATUS_READY

    def close(self, comment: str = None) -> None:
        self.context.set_state(CloseState())


class CloseState(BaseOrderState):

    status = const.OrderStatus.STATUS_CLOSED
