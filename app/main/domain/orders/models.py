import typing
import pydantic

from app.main.domain.core import models
from app.main.domain.users import models as user_models
from app.main.domain.shops import models as shop_models
from app.main.domain.orders import states, const


class Order(models.MixinCreateAndUpdateDatetime, models.MixinDeleted, pydantic.BaseModel):
    """Модель заказа."""

    __state: typing.Optional['states.OrderState'] = None
    _states: typing.Tuple = (
        states.NotSelectedState,
        states.SelectedState,
        states.SentState,
        states.AcceptState,
        states.DeniedState,
        states.StopListState,
        states.ReadyState,
        states.CloseState,
    )

    id: typing.Optional[pydantic.UUID4] = None
    sum: float
    status: const.OrderStatus
    products: typing.List['shop_models.Product']
    additives: typing.Dict[int, typing.List['shop_models.Product']] = {}
    shop_id: int
    user_id: pydantic.UUID4

    shop: typing.Optional['shop_models.Shop'] = None
    user: typing.Optional['user_models.User'] = None

    class Config:
        orm_mode = True

    def dict(self, *args, **kwargs):
        kwargs['exclude'] = {'shop', 'user', '_Order__state', '_state', '__state', *kwargs.get('exclude', ())}
        return super(Order, self).dict(*args, **kwargs)

    @property
    def _state(self):
        # FIXME: подумать можно ли обойтись без поля status в моделе и как это будет мапиться в orm
        if self.__state is not None:
            return self.__state

        _states = [state for state in self._states if state.status == self.status]

        if len(_states):
            state_class = _states[0]
            state = state_class()
            state.context = self
            self.__state = state
            return state

        return None

    def has_perm_manager(self, manager: 'shop_models.Manager') -> bool:
        """

        Args:
            manager:

        Returns:

        """

        return manager.is_active and manager.shop_id == self.shop_id

    def set_state(self, state: 'states.OrderState') -> None:
        """
        Изменяет состояние (статус) заказа.
        Args:
            state (states.OrderState): Состояние.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self.status = state.status
        state.context = self
        self.__state = state

    def add_product(self, product: 'shop_models.Product') -> None:
        """
        Добовить продукт к текущему заказу.
        Args:
            product (shop_models.Product): Продукт.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.add_product(product)

    def delete_product(self, product: 'shop_models.Product') -> None:
        """
        Удалить продукт из текущего заказа.
        Args:
            product (shop_models.Product): Продукт.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.delete_product(product)

    def send_to_managers(self) -> None:
        """
        Отправить заказ менеджеру магазина.
        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.send_to_managers()

    def accept_from_manager(self, manager: 'shop_models.Manager', bot: 'shop_models.Bot', comment: str = None) -> None:
        """
        Подтвердить заказ.
        Args:
            manager (shop_models.Manager): Менеджер.
            bot (shop_models.Bot): Бот клиента.
            comment (str): Комментарий.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.accept_from_manager(manager, bot, comment)

    def denied_from_manager(self, manager: 'shop_models.Manager', comment: str = None) -> None:
        """
        Отклонить заказ.
        Args:
            manager (shop_models.Manager): Менеджер.
            comment (str): Комментарий.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.denied_from_manager(manager, comment)

    def stop_list_from_manager(self, manager: 'shop_models.Manager', comment: str = None) -> None:
        """
        Отклонить заказ, по причине отсутсвия товара.
        Args:
            manager (shop_models.Manager): Менеджер.
            comment (str): Комментарий.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.stop_list_from_manager(manager, comment)

    def fail(self, error: str = None) -> None:
        """
        Перевести заказ в статус ошибки.
        Args:
            error (str): Комментарий.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.fail(error)

    def ready(self, comment: str = None) -> None:
        """
        Перевести заказ в статус готово.
        Args:
            comment (str): Комментарий.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.ready(comment)

    def close(self, comment: str = None) -> None:
        """
        Закрыть заказ.
        Args:
            comment (str): Комментарий.

        Returns:
            None:

        Raises:
            OrderStateMethodException: Метод не может быть вызван в данном статусе заказа.
        """
        self._state.close(comment)
