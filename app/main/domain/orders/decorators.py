from functools import wraps

from requests import exceptions as requests_exceptions

from app.core import exceptions
from app.main.domain.shops import models as shop_models

from . import exceptions as orders_exceptions


def has_perm_manager(func):

    @wraps(func)
    def wrapper(self, manager: 'shop_models.Manager', *args, **kwargs):
        if not self.context.has_perm_manager(manager):
            raise exceptions.PermissionDenied()
        func(self, manager, *args, **kwargs)
    return wrapper


def fail_state_method_error(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            func(self, *args, **kwargs)
        except orders_exceptions.OrderStateMethodException as exc:
            self.fail(error=str(exc))
    return wrapper


def fail_http_error(func):

    @wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            func(self, *args, **kwargs)
        except requests_exceptions.HTTPError as exc:
            self.fail(error=str(exc))
    return wrapper
