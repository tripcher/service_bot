class ObjectDoesNotExist(Exception):
    """Запрашиваемый объект не существует."""
    pass


class MultipleObjectsReturned(Exception):
    """Запрос возвратил несколько объектов, когда ожидался только один."""
    pass


class FailedAddObjectToCollection(Exception):
    """Не удалось добавить объект в коллекцию."""
    pass


class FailedEditObjectToCollection(Exception):
    """Не удалось изменить объект в коллекции."""
    pass


class FailedDeleteObjectToCollection(Exception):
    """Не удалось удалить объект из коллекции."""
    pass


class PermissionDenied(Exception):
    """Недостаточно прав."""
    pass
