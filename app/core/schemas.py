import pydantic


class Success(pydantic.BaseModel):
    success: bool = True
