```bash
export PYTHONPATH=.

alembic revision --autogenerate -m "Comment"

alembic upgrade head
```

### JWT для авторизации тестового coffee_bot
```bash
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJrZXkiOiIwN2M0Mjk4MjUwOWU5YTJjYmY4YWVjY2YxZjc5ZmYyYSJ9.TFGOh3WagXaszvXSL--XcMtLvywfHFTtU5Xqez9BTWE
```

### JWT для авторизации тестового manager_bot
```bash
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJrZXkiOiIzMDgwMWM4NDk3MWI1MjZmMjM0ODdlYzFkYWMwM2NhYyJ9.uWLksX2VyzTG2IIwgk5reN0xfQ_UHcDfC5QS-Q-aLY0
```

```bash
docker run -p 11211:11211 --name my-memcache -d memcached memcached -m 64
docker start my-memcache
```